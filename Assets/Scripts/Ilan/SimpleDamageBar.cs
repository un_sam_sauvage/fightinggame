﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimpleDamageBar : MonoBehaviour
{
    public Slider damageBar;
    private PlayerHealth _playerHealth;
    private Color _barColor;
    
    void Start()
    {
        _playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
        _barColor = GameObject.FindGameObjectWithTag("Bar").GetComponent<Image>().color;
    }

    private void Update()
    {
        damageBar.value = _playerHealth.health;

        if (damageBar.value <= 45)
        {
            _barColor = Color.green;
        }
        else if (damageBar.value >= 46 && damageBar.value <= 74)
        {
            _barColor = Color.yellow;
        }
        else if (damageBar.value >= 75)
        {
            _barColor = Color.red;
        }
    }
}
