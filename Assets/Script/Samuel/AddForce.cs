﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddForce : MonoBehaviour
{

    public float pourcentage;
    public float force;
    public float poids;
    public float radius;

    private float _direction;
    // Update is called once per frame
    void Update()
    {
        _direction = GetComponent<Move>().direction;
        RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, Vector2.left*_direction,radius);
        foreach (var hit in hits)
        {
            if (hit.transform.gameObject.CompareTag("Player"))
            {
                Debug.Log("yes");
                if (Input.GetKeyDown(KeyCode.A))
                {
                    hit.transform.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2((-force*pourcentage)/poids*_direction, 2);
                    pourcentage *= 1.5f;
                }
            }
        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.color =Color.blue;
        Gizmos.DrawRay(transform.position,Vector3.left*radius*_direction);
    }
}
